/*
 * Copyright (C) 2021 eric
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.dorino.pam;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 *
 * @author eric
 */
public class PamTest {

    public PamTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @AfterEach
    public void tearDown() throws Exception {
    }

    @BeforeEach
    public void setUp() throws Exception {
    }

    /*
    @Test
    public void checkLibrary() throws Exception {
        System.out.println("checkLibrary");
        assertTrue(new Pam("other").check());
    }
    */
    
    @Test
    public void testAuthenticate1() throws Exception {
        System.out.println("testAuthenticate1");
        String username = "test", credentials = "test";
        Pam pam = new Pam("other");
        Pam.Result result = pam.authenticate(username, credentials);
        System.err.println(pam.error(username, result));
        assertNotEquals(Pam.Result.PAM_SUCCESS, result);
    }

    @Test
    public void testAuthenticate2() throws Exception {
        System.out.println("testAuthenticate2");
        String username = "test";
        char[] credentials = {'t', 'e', 's', 't'};
        Pam pam = new Pam("other");
        Pam.Result result = pam.authenticate(username, credentials);
        System.err.println(pam.error(username, result));
        assertNotEquals(Pam.Result.PAM_SUCCESS, result);
    }
}
