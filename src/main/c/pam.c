/*
 * Copyright (C) 2021 eric
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <security/pam_appl.h>
#include <security/pam_misc.h>
#include <sys/types.h>
#include <ctype.h>
#include <dlfcn.h>
#include <jni.h>
#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fr_dorino_pam_Pam.h"

#define MAXUSERNAMESIZE 32
#define MAXPASSWORDSIZE 18

#define CS_SUCCESS      0
#define CS_BAD_USAGE 	-1
#define CS_BAD_DATA  	-2

static void *libpam, *libpam_misc;
static const char *servicename, *username, *password;



static int conv(int nmsg,
                const struct pam_message **msg,
                struct pam_response **resp,
                void *data) {
    int replies = 0;
    struct pam_response *reply = NULL;

    reply = (struct pam_response *)malloc(sizeof(struct pam_response)*nmsg);
    if (!reply)
        return PAM_CONV_ERR;

    for (replies = 0; replies < nmsg; replies++) {
        switch (msg[replies]->msg_style) {
            case PAM_PROMPT_ECHO_OFF:
                reply[replies].resp = strdup(password);
                break;
            default:
                reply[replies].resp = strdup("");
                break;
        }
    }
    *resp = reply;
    return PAM_SUCCESS;
}

static struct pam_conv pam_conv = { conv, NULL };

JNIEXPORT jint JNICALL JNI_OnLoad (JavaVM *vm, void *reserved) {
    libpam = dlopen("libpam.so", RTLD_GLOBAL | RTLD_LAZY);
    libpam_misc = dlopen("libpam_misc.so", RTLD_GLOBAL | RTLD_LAZY);
    return JNI_VERSION_1_4;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM *vm, void *reserved) {
    dlclose(libpam);
    dlclose(libpam_misc);
}

/*
 * Class:     fr_dorino_pam_Pam
 * Method:    authenticate
 * Signature: (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I
 */
JNIEXPORT jint JNICALL
Java_fr_dorino_pam_Pam_authenticate(JNIEnv *env, jobject obj,
                                    jstring jservicename, jstring jusername, jstring jpassword) {
    pam_handle_t *pamh = NULL;
    int retval;

    servicename = (*env)->GetStringUTFChars(env, jservicename, 0);
    username = (*env)->GetStringUTFChars(env, jusername, 0);
    password = (*env)->GetStringUTFChars(env, jpassword, 0);

    retval = pam_start(servicename, username, &pam_conv, &pamh);
    if (retval == PAM_SUCCESS) {
        pam_set_item(pamh, PAM_AUTHTOK, password);
        retval = pam_authenticate(pamh, 0);
    }
    if (retval == PAM_SUCCESS) {
        retval = pam_acct_mgmt(pamh, 0);
    }
    if (pam_end(pamh, retval) != PAM_SUCCESS)
        pamh = NULL;
    return retval;
}

/*
 * Class:     fr_dorino_pam_Pam
 * Method:    strerror
 * Signature: (Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_fr_dorino_pam_Pam_strerror(JNIEnv *env, jobject obj,
                                    jstring jservicename, jstring jusername, jint code) {
    pam_handle_t *pamh = NULL;
    int retval;
    jstring result;

    servicename = (*env)->GetStringUTFChars(env, jservicename, 0);
    username = (*env)->GetStringUTFChars(env, jusername, 0);

    retval = pam_start(servicename, username, &pam_conv, &pamh);
    if (retval == PAM_SUCCESS) {
        result = (*env)->NewStringUTF(env, pam_strerror(pamh, code));
    }
    else {
        result = (*env)->NewStringUTF(env, "?");
    }
    if (pam_end(pamh, retval) != PAM_SUCCESS)
        pamh = NULL;
    return result;
}