/*
 * Copyright (C) 2021 eric
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.dorino.pam.test;

import java.io.IOException;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.TextOutputCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.LoginContext;
import javax.security.auth.login.LoginException;

/**
 *
 * @author eric
 */
public class PamJaasTest {

    /**
     * @param args the command line arguments
     */

    private static String jaasService, username;

    public static void main(String[] args) {
        if (args.length == 1 && args[0].equals("--help")) {
                usage();
                return;
        }
        else {
            if (System.console() == null) {
                System.err.println("No console available");
                System.exit(1);
            }
            switch (args.length) {
                case 2: {
                    jaasService = args[0];
                    username = args[1];
                    break;
                }
                case 1: {
                    jaasService = "pam";
                    username = args[0];
                    break;
                }
                case 0: {
                    jaasService = System.console().readLine("JAAS service: ");
                    username = System.console().readLine("user: ");
                    break;
                }
                default:
                    usage();
                    break;
            }

            try {
                LoginContext loginContext = new LoginContext(jaasService, new PamCallbackHandler());
                loginContext.login();
                Subject subject = loginContext.getSubject();
                System.out.printf("%s:\n%s\n", jaasService, subject);
                loginContext.logout();
            } catch (LoginException e) {
                System.err.println(e.getMessage());
                System.exit(1);
            }
        }
    }

    static class PamCallbackHandler implements CallbackHandler {

        @Override
        public void handle(Callback[] callbacks)
                throws IOException, UnsupportedCallbackException {

            for (int i = 0; i < callbacks.length; i++) {
                if (callbacks[i] instanceof TextOutputCallback) {
                    TextOutputCallback toc = (TextOutputCallback) callbacks[i];
                    switch (toc.getMessageType()) {
                        case TextOutputCallback.INFORMATION:
                            System.out.println(toc.getMessage());
                            break;
                        case TextOutputCallback.ERROR:
                            System.out.println("Error: " + toc.getMessage());
                            break;
                        case TextOutputCallback.WARNING:
                            System.out.println("Warning: " + toc.getMessage());
                            break;
                        default:
                            throw new IOException("Unsupported message type: "
                                    + toc.getMessageType());
                    }

                } else if (callbacks[i] instanceof NameCallback) {
                    NameCallback c = (NameCallback) callbacks[i];
                    c.setName(username);

                } else if (callbacks[i] instanceof PasswordCallback) {
                    PasswordCallback c = (PasswordCallback) callbacks[i];
                    c.setPassword(System.console().readPassword(c.getPrompt()));

                } else
                    throw new UnsupportedCallbackException
                            (callbacks[i], "Unrecognized Callback");
            }
        }

    }

    private static void usage() {
        System.out.println("usage:");
        System.out.println("  <config> <exec> <JAAS service> <username>");
        System.out.println("  <config> <exec> <username> (JAAS service defaults to \"pam\")");
        System.out.println("  <exec>");
        System.out.println("<config> = -Djava.security.auth.login.config==src/test/resources/jaas_pam.config");
    }

}
