/*
 * Copyright (C) 2021 eric
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.dorino.pam.test;

import fr.dorino.pam.Pam;
import java.io.Console;

/**
 *
 * @author eric
 */
public class PamTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (args.length == 1 && args[0].equals("--help")) {
                usage();
                return;
        }
        else {
            Console console = System.console();
            if (console == null) {
                System.err.println("No console available");
                System.exit(1);
            }
            switch (args.length) {
                case 2: {
                    Pam pam = new Pam(args[0]);
                    System.out.println(
                        pam.error(args[1],
                                  pam.authenticate(args[1], console.readPassword("password: "))));
                    break;
                }
                case 1: {
                    Pam pam = new Pam("other");
                    System.out.println(
                        pam.error(args[0],
                                  pam.authenticate(args[1], console.readPassword("password: "))));
                    break;
                }
                case 0: {
                    Pam pam = new Pam(console.readLine("service: "));
                    String user = console.readLine("user: ");
                    System.out.println(
                        pam.error(user,
                                  pam.authenticate(user, console.readPassword("password: "))));
                    break;
                }
                default:
                    usage();
                    break;
            }
        }
    }

    private static void usage() {
        System.out.println("usage:");
        System.out.println("  <exec> <service> <username>");
        System.out.println("  <exec> <username> (service defaults to \"other\")");
        System.out.println("  <exec>");
    }
}
