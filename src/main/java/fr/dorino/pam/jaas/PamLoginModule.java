/*
 * Copyright (C) 2021 eric
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.dorino.pam.jaas;

import fr.dorino.pam.Pam;
import java.io.IOException;
import java.util.Map;
import javax.security.auth.Subject;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.auth.login.AccountExpiredException;
import javax.security.auth.login.CredentialExpiredException;
import javax.security.auth.login.FailedLoginException;
import javax.security.auth.login.LoginException;
import javax.security.auth.spi.LoginModule;

/**
 *
 * @author eric
 */
public class PamLoginModule implements LoginModule {

    private Subject subject;
    private CallbackHandler callbackHandler;
    private Map sharedState;
    private Map options;
    private Pam pam;

    @Override
    public void initialize(Subject subject, CallbackHandler callbackHandler,
                           Map<String, ?> sharedState, Map<String, ?> options) {
        this.subject = subject;
        this.callbackHandler = callbackHandler;
        this.sharedState = sharedState;
        this.options = options;
        this.pam = makePam();
    }

    private Pam makePam() {
        String serviceName = options != null ?
                (String)options.get("serviceName") :
                null;
        if (serviceName == null)
            serviceName = "other";
        return new Pam(serviceName);
    }

    private boolean loginOk = false;

    @Override
    public boolean login() throws LoginException {
        Callback[] callbacks = new Callback[2];
        NameCallback nameCallback = new NameCallback("user: ");
        PasswordCallback passwordCallback = new PasswordCallback("password: ", false);
        callbacks[0] = nameCallback;
        callbacks[1] = passwordCallback;

        loginOk = false;
        try {
            this.callbackHandler.handle(callbacks);
        } catch (NullPointerException e) {
            throw new LoginException("No callback handler supplied");
        } catch (IOException | UnsupportedCallbackException e) {
            throw new LoginException(e.getMessage());
        }
        Pam.Result result = pam.authenticate(
                nameCallback.getName(),
                passwordCallback.getPassword());
        switch (result) {
            case PAM_SUCCESS:
                return loginOk = true;
            case PAM_ACCT_EXPIRED:
                throw new AccountExpiredException(String.format(
                    "%s", pam.error(nameCallback.getName(), result)));
            case PAM_CRED_EXPIRED:
                throw new CredentialExpiredException(String.format(
                    "%s", pam.error(nameCallback.getName(), result)));
            default:
                throw new FailedLoginException(String.format(
                    "%s", pam.error(nameCallback.getName(), result)));
        }
    }

    @Override
    public boolean commit() throws LoginException {
        return loginOk;
    }

    @Override
    public boolean abort() throws LoginException {
        return true;
    }

    @Override
    public boolean logout() throws LoginException {
        return false;
    }

}
