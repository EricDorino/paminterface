/*
 * Copyright (C) 2021 eric
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.dorino.pam;

import java.io.IOException;

/**
 *
 * @author eric
 */
public class Pam {

    private static final String LIBRARY_NAME = "javapam";

    static {
        try {
            LibraryLoader.loadLibrary(LIBRARY_NAME);
        } catch (IOException e) {
            System.err.println(e);
            System.exit(1);
        }
    }

    private final String serviceName;

    public Pam(String serviceName) {
        if (serviceName == null)
            throw new NullPointerException("Null PAM Service name");
        else if (serviceName.isEmpty())
            throw new IllegalArgumentException("Empty PAM Service name");
        this.serviceName = serviceName;
    }

    private native int authenticate(String serviceName,
                                    String username,
                                    String credentials);
    private native String strerror(String serviceName,
                                   String username,
                                   int code);

    public Result authenticate(String username, String credentials)
                throws NullPointerException {
        if (username == null)
            throw new NullPointerException("Null username");
        else if (credentials == null)
            throw new NullPointerException("Null credentials");
        synchronized (Pam.class) {
            return Result.valueFrom(authenticate(serviceName, username, credentials));
        }
    }

    public Result authenticate(String username, char[] credentials)
                throws NullPointerException {
        return authenticate(username, String.valueOf(credentials));
    }

    public boolean isAuthenticated(String username, String credentials) {
        return Result.PAM_SUCCESS == authenticate(username, credentials);
    }

    public boolean isAuthenticated(String username, char[] credentials) {
        return Result.PAM_SUCCESS == authenticate(username, credentials);
    }

    public String error(String username, Result code) {
        synchronized (Pam.class) {
            return strerror(serviceName, username, code.ordinal());
        }
    }

    public String getServiceName() {
        return serviceName;
    }

    public enum Result {
        PAM_SUCCESS,
        PAM_OPEN_ERR,
        PAM_SYMBOL_ERR,
        PAM_SERVICE_ERR,
        PAM_SYSTEM_ERR,
        PAM_BUF_ERR,
        PAM_PERM_DENIED,
        PAM_AUTH_ERR,
        PAM_CRED_INSUFFICIENT,
        PAM_AUTHINFO_UNAVAIL,
        PAM_USER_UNKNOWN,
        PAM_MAXTRIES,
        PAM_NEW_AUTHTOK_REQD,
        PAM_ACCT_EXPIRED,
        PAM_SESSION_ERR,
        PAM_CRED_UNAVAIL,
        PAM_CRED_EXPIRED,
        PAM_CRED_ERR,
        PAM_NO_MODULE_DATA,
        PAM_CONV_ERR,
        PAM_AUTHTOK_ERR,
        PAM_AUTHTOK_RECOVER_ERR,
        PAM_AUTHTOK_LOCK_BUSY,
        PAM_AUTHTOK_DISABLE_AGING,
        PAM_TRY_AGAIN,
        PAM_IGNORE,
        PAM_ABORT,
        PAM_AUTHTOK_EXPIRED,
        PAM_MODULE_UNKNOWN,
        PAM_BAD_ITEM,
        PAM_CONV_AGAIN,
        PAM_INCOMPLETE;

        public static Result valueFrom(int index) {
            return Result.values()[index];
        }
    }
}
